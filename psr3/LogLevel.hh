<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace
{
	class LogLevel
	{
		const string EMERGENCY	='emergency';
		const string ALERT		='alert';
		const string CRITICAL	='critical';
		const string ERROR		='error';
		const string WARNING	='warning';
		const string NOTICE		='notice';
		const string INFO		='info';
		const string DEBUG		='debug';
	}
}
