<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\logger
{
	use nuclio\core\ClassManager;
	use nuclio\core\plugin\Plugin;
	use nuclio\plugin\email\EmailException;
	use nuclio\plugin\config\ConfigCollection;
	use monolog\src\Monolog\Processor\WebProcessor;
	use monolog\src\Monolog\Handler\TestHandler;
	use nuclio\plugin\logger\psr3\LoggerInterface as LoggerCommonInterface;
	use nuclio\plugin\provider\manager\Manager as ProviderManager;
	use nuclio\plugin\logger\exception\LoggerException;
	use nuclio\plugin\logger\psr3\LoggerInterface;
	
	<<factory>>
	class Logger extends Plugin implements LoggerCommonInterface
	{
		
		private string $driverName;
		public LoggerCommonInterface $driver;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Logger
		{
			$instance=ClassManager::getClassInstance(self::class,...$args);
			return ($instance instanceof self)?$instance:new self(...$args);
		}

		public function __construct(Map<string,string> $options)
		{
			parent::__construct();
			
			$channel = $options->get('channel');
			$log = $channel['logger'];
			$driverName = $channel['driver']; 
			
			if (is_null($driverName) || !is_string($driverName))
			{
				throw new LoggerException('Unable to load driver for logger. Driver must be provided as a string.');
			}
			$this->driverName=$driverName;

			$driverIntance=ProviderManager::request('logger::'.$this->driverName,$options);

			if ($driverIntance instanceof LoggerCommonInterface)
			{
				$this->driver=$driverIntance;
			}
			else
			{
				throw new LoggerException(sprintf('Unable to load driver for logger. Driver "%s" is not available.',$this->driverName));
			}
		}

		public function getDriverName():string
		{
			return $this->driverName;
		}

		public function getDriver():this
		{
			return $this->driver;
		}
		
		/**
		 * @inheritdoc
		 */
		public function emergency(string $message, array<mixed> $context=[]):this
		{
			return $this->driver->emergency($message, $context);
		}
		
		/**
		 * @inheritdoc
		 */
		public function alert(string $message, array<mixed> $context=[]):this
		{
			return $this->driver->alert($message, $context);
		}
		
		/**
		 * @inheritdoc
		 */
		public function critical(string $message, array<mixed> $context=[]):this
		{
			return $this->driver->critical($message, $parameter);
		}
		
 		/**
		 * @inheritdoc
		 */
		public function error(string $message, array<mixed> $context=[]):this
		{
			return $this->driver->error($message, $context);
		}
		
		/**
		 * @inheritdoc
		 */
		public function warning(string $message, array<mixed> $context=[]):this
		{
			return $this->driver->warning($message, $context);
		}
		
		/**
		 * @inheritdoc
		 */
		public function notice(string $message, array<mixed> $context=[]):this
		{
			return $this->driver->notice($message, $context);
		}
		
		/**
		 * @inheritdoc
		 */
		public function info(string $message, array<mixed> $context=[]):this
		{
			$this->driver->info($message, $context);
			return $this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function debug(string $message, array<mixed> $context=[]):this
		{
			$this->driver->debug($message, $context);
			return $this;
		}
		
		/**
		 * @inheritdoc
		 */
		public function log(int $level, $message, array<mixed> $context=[]):this
		{
			return $this->driver->log($message, $context);
		}
		
		public function __call(string $method, array<mixed> $args):mixed
		{
			return call_user_func_array([$this->driver,$method],$args);
		}
	}
}
